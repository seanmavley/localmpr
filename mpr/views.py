from django.shortcuts import render
from django.db.models import Q
from mpr.models import Directory

# Create your views here.
def home(request):
	return render(request, 'index.html')

def search(request):
	query = request.GET.get('q', '')
	if query:
		qset = Q(name__icontains=query,) 
		results = Directory.objects.filter(qset).distinct()
	else:
		results = []
		
	return render(request, "results.html", {
		'results': results,
		'query': query,
	})