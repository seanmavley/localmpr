from django.conf.urls import patterns, include, url
from . import views
from django.conf.urls.static import static
# from LocalMPR import settings

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),
    url(r'^result/', views.search, name='search'),
) 
#+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)