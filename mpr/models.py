from django.db import models

# Create your models here.
class Directory(models.Model):
	name = models.CharField(max_length=500)
	max_price = models.CharField(max_length=500)
	sch = models.CharField(max_length=500)
	dosage = models.CharField(max_length=500)
	pack_quantity = models.CharField(max_length=500)
	number_of_packs = models.CharField(max_length=500)
	generic = models.CharField(max_length=500)
	ingredients = models.CharField(max_length=500)

	def __unicode__(self):
		return self.name
