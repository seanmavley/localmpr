Medicine Price Registry
=======================

You can find a running instance at http://localmpr.herokuapp.com.

A typical use-case would be for a consumer to look for alternative products for a particular medicine, often generic medicines can be much cheaper than the branded product.
Another use-case allows consumers to ensure that they are not being overcharged for their medicines. This database publishes the maximum price at which a medicine can be sold. A pharmacy cannot legally increase the price of a particular medicine above the price listed here.

Contributing
============


Deployment
==========


TODO
====

* None so far

Contributors
============
- Rexford google.com/+Nkansahrexford
